require("dotenv").config();
const sgMail = require("@sendgrid/mail");
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

exports.mailer = async (req, res) => {
    res.set("Access-Control-Allow-Origin", "*");
    res.set("Access-Control-Allow-Headers", "Content-Type");
    res.set("Access-Control-Allow-Methods", "GET, POST");

    if (req.method === "POST") {
        let resp = {};
        const { to, subject, cc, mailBody } = req.body;
        const msg = {
            to: to,
            from: process.env.EMAIL_SENDER_FROM, // Use the email address or domain you verified above
            subject: subject,
            cc: cc,
            html: mailBodygit,
        };
        try {
            const emailRes = await sgMail.send(msg);

            resp = {
                statusCode: 200,
                message: "Email sent. Check your email",
                data: emailRes,
            };
            return res.status(200).json(resp);
        } catch (error) {
            console.log(error);
            resp = {
                statusCode: 400,
                message: "Something went wrong!",
                error,
            };
            return res.status(400).json(resp);
        }
    } else if (req.method === "OPTIONS") {
        res.set("Access-Control-Allow-Methods", "GET");
        res.set("Access-Control-Max-Age", "3600");

        res.status(204).send("");
        return;
    } else {
        let ob = {
            statusCode: 400,
            message: "Method not supported",
        };
        return res.status(400).json(ob);
    }
};
